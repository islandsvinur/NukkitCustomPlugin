package nl.nogates;

import cn.nukkit.event.Listener;
import cn.nukkit.plugin.PluginBase;

public class NukkitCustomPlugin extends PluginBase implements Listener {

    private static NukkitCustomPlugin instance;

    public static NukkitCustomPlugin getInstance() {
        return instance;
    }

    @Override
    public void onLoad() {
        instance = this;
    }

    @Override
    public void onEnable() {
        this.getServer().getPluginManager().registerEvents(new EventListener(), this);
    }
}
