package nl.nogates;

import cn.nukkit.entity.Entity;
import cn.nukkit.event.EventHandler;
import cn.nukkit.event.Listener;
import cn.nukkit.event.entity.EntityExplosionPrimeEvent;
import cn.nukkit.level.Level;
import cn.nukkit.level.particle.HeartParticle;
import nukkitcoders.mobplugin.entities.monster.walking.Creeper;
import nukkitcoders.mobplugin.utils.Utils;

public class EventListener implements Listener {

    @EventHandler(ignoreCancelled = true)
    public void EntityExplosionPrimeEvent(EntityExplosionPrimeEvent ev) {
        if (ev.getEntity() instanceof Creeper) {
            ev.setCancelled();
            Entity entity = ev.getEntity();
            Level level = entity.level;
            for (int i = 0; i < 10; i++) {
                level.addParticle(new HeartParticle(entity.add(Utils.rand(-1.0, 1.0), (entity.getHeight() * 0.75F) + Utils.rand(-1.0, 1.0), Utils.rand(-1.0, 1.0))));
            }
        }
    }
}
